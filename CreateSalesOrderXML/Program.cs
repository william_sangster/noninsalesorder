﻿using System;
using System.Data;
using System.Xml;

namespace CreateSalesOrderXML
{
    class Program
    {
        static void Main(string[] args)
        {
            

            XmlTextWriter writer = new XmlTextWriter("Order.xml", System.Text.Encoding.UTF8);
            writer.WriteStartDocument(true);
            writer.Formatting = Formatting.Indented;
            writer.Indentation = 2;
            writer.WriteStartElement("Envelope", "http://schemas.microsoft.com/dynamics/2011/01/documents/Message");

            writer.WriteStartElement("Header");
                writer.WriteStartElement("MessageId");
                Guid g = Guid.NewGuid();
                writer.WriteString("{" + g.ToString().ToUpper() + "}");
                                  
                writer.WriteEndElement();

                writer.WriteStartElement("LogonAsUser");
                writer.WriteString(@"HDQTRS\bill.sangster");
                writer.WriteEndElement();

                writer.WriteStartElement("Company");
                writer.WriteString("NMI");
                writer.WriteEndElement();

                writer.WriteStartElement("Action");
                writer.WriteString("http://schemas.microsoft.com/dynamics/2008/01/services/SalesOrderService/create");
                writer.WriteEndElement();

             writer.WriteEndElement();//Header

            writer.WriteStartElement("Body");
            writer.WriteStartElement("MessageParts");
            writer.WriteStartElement("SalesOrder", "http://schemas.microsoft.com/dynamics/2008/01/documents/SalesOrder");
            writer.WriteStartElement("SalesTable");
            writer.WriteAttributeString("class", "entity");
            createSingleNode("CustAccount", "108269",writer);
            createSingleNode("CustomerRef", "2362901", writer);
            createSingleNode("Deadline", "2020-03-25", writer);
            createDeliveryPostalAddress("000020963", "2016-01-28T20:18:15.000Z", writer);
            createSingleNode("PurchOrderFormNum", "EDI", writer);
            createSingleNode("ReceiptDateRequested", "2020-03-25", writer);
            createSingleNode("SalesOriginId", "EDI", writer);
            createSingleNode("ShippingDateRequested", "2020-03-25", writer);
            createSalesLine("1", "4774-000","4.85","7.00","2020-03-25",writer);
            createSalesLine("2", "4774-000", "278.00", "1.00", "2020-03-25", writer);


            writer.WriteEndElement();//Sales Table
            writer.WriteEndElement();//Sales Order
            writer.WriteEndElement();//MessageParts
            writer.WriteEndElement();//Body
            writer.WriteEndElement();//Envelope
            writer.WriteEndDocument();
            writer.Close();
            Console.WriteLine("XML File created ! ");

        }
        private static void createNode(string pID, string pName, string pPrice, XmlTextWriter writer)
        {
            writer.WriteStartElement("Product");
            writer.WriteStartElement("Product_id");
            writer.WriteString(pID);
            writer.WriteEndElement();
            writer.WriteStartElement("Product_name");
            writer.WriteString(pName);
            writer.WriteEndElement();
            writer.WriteStartElement("Product_price");
            writer.WriteString(pPrice);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        private static void createSingleNode(string nodeName, string value,  XmlTextWriter writer)
        {
            writer.WriteStartElement(nodeName);           
            writer.WriteString(value);         
            writer.WriteEndElement();
        }

        private static void createDeliveryPostalAddress(string locationId, string UTCDate, XmlTextWriter writer)
        {
            writer.WriteStartElement("DeliveryPostalAddress");
            writer.WriteStartElement("Location_LocationId", "http://schemas.microsoft.com/dynamics/2008/01/sharedtypes");
            writer.WriteString(locationId);
            writer.WriteEndElement();
            writer.WriteStartElement("ValidFrom", "http://schemas.microsoft.com/dynamics/2008/01/sharedtypes");
            writer.WriteString(UTCDate);
            writer.WriteEndElement();
            writer.WriteEndElement();
        }

        private static void createSalesLine(string lineNumber, string itemId, string salesPrice, string quantity, string ShippingDateRequested, XmlTextWriter writer)
        {
            writer.WriteStartElement("SalesLine");
            writer.WriteAttributeString("class", "entity");
            writer.WriteStartElement("CustomerLineNum");
            writer.WriteString(lineNumber);
            writer.WriteEndElement();

            writer.WriteStartElement("ExternalItemId");
            writer.WriteEndElement();

            writer.WriteStartElement("ItemId");
            writer.WriteString(itemId);
            writer.WriteEndElement();

            writer.WriteStartElement("SalesPrice");
            writer.WriteString(salesPrice.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("SalesQty");
            writer.WriteString(quantity.ToString());
            writer.WriteEndElement();

            writer.WriteStartElement("SalesUnit");
            writer.WriteString("ea");
            writer.WriteEndElement();

            writer.WriteStartElement("ShippingDateRequested");
            writer.WriteString(ShippingDateRequested.ToString());
            writer.WriteEndElement();

            writer.WriteEndElement();
        }
    }
}
