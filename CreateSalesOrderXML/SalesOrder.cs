﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CreateSalesOrderXML
{
    public class SalesOrder
    {
        public string SalesId { get; set; }
        public Guid MessageId { get; set; }
        public string CustAccount { get; set; }
        public string CustomerRef { get; set; }
        public DateTime Deadline { get; set; }
        public string LocationId { get; set; }
        public DateTime AddressEffectiveDate { get; set; }
        public string PurchaseOrderFormNum { get; set; }
        public DateTime ReceiptDateRequested { get; set; }
        public string SalesOriginId { get; set; }
        public DateTime ShipDateRequested { get; set; }

    }

    public class SalesLine
    {
        public string CustomerLineNumber { get; set; }
        public string ExternalItemId { get; set; }
        public string ItemId { get; set; }
        public decimal SalesPrice { get; set; }
        public decimal SalesQuantity { get; set; }
        public string SalesUnit { get; set; }

    }
}
